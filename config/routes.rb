Rails.application.routes.draw do
  get '*link/:id', to: 'sub_pages#show', constraints: SubPageConstraint, as: :sub_page_link
  get '*link', to: 'menu_entities#show', constraints: MenuEntityConstraint, as: :menu_entity_link

  resources :menu_entities
  resources :sub_pages
  resources :contacts, only: [:index, :create]
  resources :order_services, only: [:new, :create]
  mount LetterOpenerWeb::Engine, at: "/letter_opener"

  root 'welcome#index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
