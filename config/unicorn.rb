APP_ROOT = File.expand_path(File.dirname(File.dirname(__FILE__)))

worker_processes 3
working_directory APP_ROOT
listen APP_ROOT + '/tmp/sockets/unicorn.sock'
listen '0.0.0.0:3000',  tcp_nopush: true
pid APP_ROOT + '/tmp/pids/unicorn.pid'
stderr_path APP_ROOT + '/log/unicorn.stderr.log'
stdout_path APP_ROOT + '/log/unicorn.stdout.log'

timeout 60

