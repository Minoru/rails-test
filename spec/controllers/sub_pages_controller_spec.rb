require 'rails_helper'

RSpec.describe SubPagesController, type: :controller do
  let(:valid_attributes) {
    {
      name: 'Подпункт меню',
      text: 'text',
      menu_entity_id: menu_entity.id
    }
  }

  let(:invalid_attributes) {
    {
      name: '',
      text: '',
      menu_entity_id: ''
    }
  }

  let(:menu_entity) do
    MenuEntity.create!(
      :name => "Услуга",
      :link => "link",
      :text => "text"
    )
  end

  let(:valid_session) { {} }

  describe "GET #show" do
    it "assigns the requested sub_page as @sub_page" do
      sub_page = SubPage.create! valid_attributes
      get :show, params: {id: sub_page.to_param}, session: valid_session
      expect(assigns(:sub_page)).to eq(sub_page)
    end
  end

  describe "GET #new" do
    it "assigns a new sub_page as @sub_page" do
      get :new, params: {}, session: valid_session
      expect(assigns(:sub_page)).to be_a_new(SubPage)
    end
  end

  describe "GET #edit" do
    it "assigns the requested sub_page as @sub_page" do
      sub_page = SubPage.create! valid_attributes
      get :edit, params: {id: sub_page.to_param}, session: valid_session
      expect(assigns(:sub_page)).to eq(sub_page)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new SubPage" do
        expect {
          post :create, params: {sub_page: valid_attributes}, session: valid_session
        }.to change(SubPage, :count).by(1)
      end

      it "assigns a newly created sub_page as @sub_page" do
        post :create, params: {sub_page: valid_attributes}, session: valid_session
        expect(assigns(:sub_page)).to be_a(SubPage)
        expect(assigns(:sub_page)).to be_persisted
      end

      it "redirects to the created sub_page" do
        post :create, params: {sub_page: valid_attributes}, session: valid_session
        expect(response).to redirect_to(sub_page_link_url(menu_entity.link, SubPage.last))
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved sub_page as @sub_page" do
        post :create, params: {sub_page: invalid_attributes}, session: valid_session
        expect(assigns(:sub_page)).to be_a_new(SubPage)
      end

      it "re-renders the 'new' template" do
        post :create, params: {sub_page: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          name: 'Подпункт меню2',
          text: 'text',
          menu_entity_id: menu_entity.id
        }
      }

      it "updates the requested sub_page" do
        sub_page = SubPage.create! valid_attributes
        put :update, params: {id: sub_page.to_param, sub_page: new_attributes}, session: valid_session
        sub_page.reload
        expect(sub_page.name).to eq(new_attributes[:name])
      end

      it "assigns the requested sub_page as @sub_page" do
        sub_page = SubPage.create! valid_attributes
        put :update, params: {id: sub_page.to_param, sub_page: valid_attributes}, session: valid_session
        expect(assigns(:sub_page)).to eq(sub_page)
      end

      it "redirects to the sub_page" do
        sub_page = SubPage.create! valid_attributes
        put :update, params: {id: sub_page.to_param, sub_page: valid_attributes}, session: valid_session
        expect(response).to redirect_to(sub_page_link_url(menu_entity.link, SubPage.last))
      end
    end

    context "with invalid params" do
      it "assigns the sub_page as @sub_page" do
        sub_page = SubPage.create! valid_attributes
        put :update, params: {id: sub_page.to_param, sub_page: invalid_attributes}, session: valid_session
        expect(assigns(:sub_page)).to eq(sub_page)
      end

      it "re-renders the 'edit' template" do
        sub_page = SubPage.create! valid_attributes
        put :update, params: {id: sub_page.to_param, sub_page: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested sub_page" do
      sub_page = SubPage.create! valid_attributes
      expect {
        delete :destroy, params: {id: sub_page.to_param}, session: valid_session
      }.to change(SubPage, :count).by(-1)
    end

    it "redirects to the sub_pages list" do
      sub_page = SubPage.create! valid_attributes
      delete :destroy, params: {id: sub_page.to_param}, session: valid_session
      expect(response).to redirect_to(menu_entities_url)
    end
  end

end
