require 'rails_helper'

RSpec.describe OrderServicesController, type: :controller do
  let(:valid_attributes) {
    {
      full_name: 'Фамилия Имя Отчество',
      phone: '+71111111111',
      email: 'test@email.com',
      comment: 'комментарий',
      menu_entity_ids: [menu_entity]
    }
  }

  let(:invalid_attributes) {
    {
      full_name: 'Фамилия Имя',
      phone: '+7111111111',
      email: 'test',
      comment: '',
    }
  }

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # OrderServicesController. Be sure to keep this updated too.
  let(:valid_session) { {} }

  describe "GET #new" do
    it "assigns a new order_service as @order_service" do
      get :new, params: {}, session: valid_session
      expect(assigns(:order_service)).to be_a_new(OrderService)
    end
  end

  describe "POST #create" do
    let!(:menu_entity) do
      MenuEntity.create!(
        :name => "Услуга",
        :link => "link",
        :text => "text"
      )
    end

    context "with valid params" do
      it "creates a new OrderService" do
        expect {
          post :create, params: {order_service: valid_attributes}, session: valid_session
        }.to change(OrderService, :count).by(1)
      end

      it "assigns a newly created order_service as @order_service" do
        post :create, params: {order_service: valid_attributes}, session: valid_session
        expect(assigns(:order_service)).to be_a(OrderService)
        expect(assigns(:order_service)).to be_persisted
      end

      it "redirects to the created order_service" do
        post :create, params: {order_service: valid_attributes}, session: valid_session
        expect(response).to redirect_to(new_order_service_path)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved order_service as @order_service" do
        post :create, params: {order_service: invalid_attributes}, session: valid_session
        expect(assigns(:order_service)).to be_a_new(OrderService)
      end

      it "re-renders the 'new' template" do
        post :create, params: {order_service: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end
end
