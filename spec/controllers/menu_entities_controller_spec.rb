require 'rails_helper'

RSpec.describe MenuEntitiesController, type: :controller do
  let(:valid_attributes) {
    {
      name: 'О компании',
      link: 'test',
      text: 'text',
    }
  }

  let(:invalid_attributes) {
    {
      name: '',
      link: '',
      text: '',
    }
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all menu_entities as @menu_entities" do
      menu_entity = MenuEntity.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:menu_entities)).to eq([menu_entity])
    end
  end

  describe "GET #show" do
    it "assigns the requested menu_entity as @menu_entity" do
      menu_entity = MenuEntity.create! valid_attributes
      get :show, params: {link: menu_entity.link}, session: valid_session
      expect(assigns(:menu_entity)).to eq(menu_entity)
    end
  end

  describe "GET #new" do
    it "assigns a new menu_entity as @menu_entity" do
      get :new, params: {}, session: valid_session
      expect(assigns(:menu_entity)).to be_a_new(MenuEntity)
    end
  end

  describe "GET #edit" do
    it "assigns the requested menu_entity as @menu_entity" do
      menu_entity = MenuEntity.create! valid_attributes
      get :edit, params: {id: menu_entity.to_param}, session: valid_session
      expect(assigns(:menu_entity)).to eq(menu_entity)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new MenuEntity" do
        expect {
          post :create, params: {menu_entity: valid_attributes}, session: valid_session
        }.to change(MenuEntity, :count).by(1)
      end

      it "assigns a newly created menu_entity as @menu_entity" do
        post :create, params: {menu_entity: valid_attributes}, session: valid_session
        expect(assigns(:menu_entity)).to be_a(MenuEntity)
        expect(assigns(:menu_entity)).to be_persisted
      end

      it "redirects to the created menu_entity" do
        post :create, params: {menu_entity: valid_attributes}, session: valid_session
        expect(response).to redirect_to(menu_entity_link_url(MenuEntity.last.link))
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved menu_entity as @menu_entity" do
        post :create, params: {menu_entity: invalid_attributes}, session: valid_session
        expect(assigns(:menu_entity)).to be_a_new(MenuEntity)
      end

      it "re-renders the 'new' template" do
        post :create, params: {menu_entity: invalid_attributes}, session: valid_session
        expect(response).to render_template("new")
      end
    end
  end

  describe "PUT #update" do
    context "with valid params" do
      let(:new_attributes) {
        {
          name: 'Контакты',
          link: 'test',
          text: 'text',
        }
      }

      it "updates the requested menu_entity" do
        menu_entity = MenuEntity.create! valid_attributes
        put :update, params: {id: menu_entity.to_param, menu_entity: new_attributes}, session: valid_session
        menu_entity.reload
        expect(menu_entity.name).to eq(new_attributes[:name])
      end

      it "assigns the requested menu_entity as @menu_entity" do
        menu_entity = MenuEntity.create! valid_attributes
        put :update, params: {id: menu_entity.to_param, menu_entity: valid_attributes}, session: valid_session
        expect(assigns(:menu_entity)).to eq(menu_entity)
      end

      it "redirects to the menu_entity" do
        menu_entity = MenuEntity.create! valid_attributes
        put :update, params: {id: menu_entity.to_param, menu_entity: valid_attributes}, session: valid_session
        expect(response).to redirect_to(menu_entity_link_url(menu_entity.link))
      end
    end

    context "with invalid params" do
      it "assigns the menu_entity as @menu_entity" do
        menu_entity = MenuEntity.create! valid_attributes
        put :update, params: {id: menu_entity.to_param, menu_entity: invalid_attributes}, session: valid_session
        expect(assigns(:menu_entity)).to eq(menu_entity)
      end

      it "re-renders the 'edit' template" do
        menu_entity = MenuEntity.create! valid_attributes
        put :update, params: {id: menu_entity.to_param, menu_entity: invalid_attributes}, session: valid_session
        expect(response).to render_template("edit")
      end
    end
  end

  describe "DELETE #destroy" do
    it "destroys the requested menu_entity" do
      menu_entity = MenuEntity.create! valid_attributes
      expect {
        delete :destroy, params: {id: menu_entity.to_param}, session: valid_session
      }.to change(MenuEntity, :count).by(-1)
    end

    it "redirects to the menu_entities list" do
      menu_entity = MenuEntity.create! valid_attributes
      delete :destroy, params: {id: menu_entity.to_param}, session: valid_session
      expect(response).to redirect_to(menu_entities_url)
    end
  end

end
