require 'rails_helper'

RSpec.describe ContactsController, type: :controller do
  let(:valid_attributes) {
    {
      full_name: 'Фамилия Имя Отчество',
      phone: '+71111111111',
      email: 'test@email.com',
    }
  }

  let(:invalid_attributes) {
    {
      full_name: 'Фамилия Имя',
      phone: '+7111111111',
      email: 'test',
    }
  }

  let(:valid_session) { {} }

  describe "GET #index" do
    it "assigns all contacts as @contacts" do
      contact = Contact.create! valid_attributes
      get :index, params: {}, session: valid_session
      expect(assigns(:contacts)).to eq([contact])
      expect(assigns(:contact)).to be_a_new(Contact)
    end
  end

  describe "POST #create" do
    context "with valid params" do
      it "creates a new Contact" do
        expect {
          post :create, params: {contact: valid_attributes}, session: valid_session
        }.to change(Contact, :count).by(1)
      end

      it "assigns a newly created contact as @contact" do
        post :create, params: {contact: valid_attributes}, session: valid_session
        expect(assigns(:contact)).to be_a(Contact)
        expect(assigns(:contact)).to be_persisted
      end

      it "redirects to the created contact" do
        post :create, params: {contact: valid_attributes}, session: valid_session
        expect(response).to redirect_to(contacts_path)
      end
    end

    context "with invalid params" do
      it "assigns a newly created but unsaved contact as @contact" do
        post :create, params: {contact: invalid_attributes}, session: valid_session
        expect(assigns(:contact)).to be_a_new(Contact)
      end

      it "re-renders the 'new' template" do
        post :create, params: {contact: invalid_attributes}, session: valid_session
        expect(response).to render_template("index")
      end
    end
  end
end
