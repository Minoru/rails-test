class MenuEntityConstraint
  def self.matches?(request)
    MenuEntity.find_by_link(request.path_parameters[:link]).present?
  end
end
