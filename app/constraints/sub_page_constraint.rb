class SubPageConstraint
  def self.matches?(request)
    menu_entity = MenuEntity.find_by_link(request.path_parameters[:link])
    menu_entity.present? && request.path_parameters[:id].present? && menu_entity.sub_pages.find(request.path_parameters[:id])
  end
end
