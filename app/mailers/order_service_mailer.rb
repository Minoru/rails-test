class OrderServiceMailer < ApplicationMailer
  def notify(order_service)
    @order_service = order_service
    mail(to: @order_service.email, subject: 'Заказ услуги')
  end
end
