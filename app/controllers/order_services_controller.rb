class OrderServicesController < ApplicationController
  def new
    @order_service = OrderService.new
  end

  def create
    @order_service = OrderService.new(order_service_params)
    if @order_service.save
      OrderServiceMailer.notify(@order_service).deliver_later
      redirect_to new_order_service_path, notice: 'Спасибо'
    else
      render :new
    end
  end

  private
    def order_service_params
      params.require(:order_service).permit(:full_name, :phone, :email, :comment, {menu_entity_ids: []})
    end
end
