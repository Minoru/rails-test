class SubPagesController < ApplicationController
  before_action :set_sub_page, only: [:show, :edit, :update, :destroy]

  def show
  end

  def new
    @sub_page = SubPage.new
  end

  def edit
  end

  def create
    @sub_page = SubPage.new(sub_page_params)

    if @sub_page.save
      redirect_to sub_page_link_url(@sub_page.menu_entity.link, @sub_page.id)
    else
      render :new
    end
  end

  def update
    if @sub_page.update(sub_page_params)
      redirect_to sub_page_link_url(@sub_page.menu_entity.link, @sub_page.id)
    else
      render :edit
    end
  end

  def destroy
    @sub_page.destroy
    redirect_to menu_entities_url
  end

  private
    def set_sub_page
      @sub_page = SubPage.find(params[:id])
    end

    def sub_page_params
      params.require(:sub_page).permit(:name, :text, :menu_entity_id)
    end
end
