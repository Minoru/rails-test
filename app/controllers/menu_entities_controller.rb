class MenuEntitiesController < ApplicationController
  before_action :set_menu_entity, only: [:edit, :update, :destroy]

  def index
    @menu_entities = MenuEntity.includes(:sub_pages).all
  end

  def show
    @menu_entity = MenuEntity.find_by_link(params[:link])
  end

  def new
    @menu_entity = MenuEntity.new
  end

  def edit
  end

  def create
    @menu_entity = MenuEntity.new(menu_entity_params)

    if @menu_entity.save
      redirect_to menu_entity_link_url(@menu_entity.link)
    else
      render :new
    end
  end

  def update
    if @menu_entity.update(menu_entity_params)
      redirect_to menu_entity_link_url(@menu_entity.link)
    else
      render :edit
    end
  end

  def destroy
    @menu_entity.destroy
    redirect_to menu_entities_url
  end

  private
    def set_menu_entity
      @menu_entity = MenuEntity.find(params[:id])
    end

    def menu_entity_params
      params.require(:menu_entity).permit(:name, :link, :text)
    end
end
