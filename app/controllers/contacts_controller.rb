class ContactsController < ApplicationController
  before_action :set_contacts, only: [:index, :create]

  def index
    @contact = Contact.new
  end

  def create
    @contact = Contact.new(contact_params)

    if @contact.save
      redirect_to contacts_path
    else
      render :index
    end
  end

  private
    def set_contacts
      @contacts = Contact.all
    end

    def contact_params
      params.require(:contact).permit(:full_name, :phone, :email)
    end
end
