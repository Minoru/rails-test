class MenuEntity < ApplicationRecord
  VALID_LINK_REGEX = /\A[-a-zA-Z0-9]*\z/

  validates :name, :link, :text, presence: true
  validates :link, uniqueness: true, format: { with: VALID_LINK_REGEX }

  has_many :sub_pages, dependent: :destroy
  has_and_belongs_to_many :order_services

  scope :with_subpages, -> { includes(:sub_pages).where.not(sub_pages: { id: nil }) }
  scope :ordered, -> { order :name }
end
