class Contact < ApplicationRecord
  FULL_NAME_REGEXP = /[а-яА-Я]+\s+[а-яА-Я]+\s+[а-яА-Я]+/
  validates :full_name, :phone, :email, presence: true
  validates :email, email: true
  validates :phone, phony_plausible: true
  validates :full_name, format: { with: FULL_NAME_REGEXP }

  phony_normalize :phone, default_country_code: 'RU'
end
