class SubPage < ApplicationRecord
  belongs_to :menu_entity

  validates :name, :text, :menu_entity_id, presence: true
end
