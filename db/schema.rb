# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160724134848) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "contacts", force: :cascade do |t|
    t.string   "full_name"
    t.string   "phone"
    t.string   "email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "menu_entities", force: :cascade do |t|
    t.string   "name",       null: false
    t.string   "link",       null: false
    t.text     "text",       null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["link"], name: "index_menu_entities_on_link", using: :btree
  end

  create_table "menu_entities_order_services", id: false, force: :cascade do |t|
    t.integer "menu_entity_id"
    t.integer "order_service_id"
    t.index ["menu_entity_id"], name: "index_menu_entities_order_services_on_menu_entity_id", using: :btree
    t.index ["order_service_id"], name: "index_menu_entities_order_services_on_order_service_id", using: :btree
  end

  create_table "order_services", force: :cascade do |t|
    t.string   "full_name",  null: false
    t.string   "phone",      null: false
    t.string   "email",      null: false
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sub_pages", force: :cascade do |t|
    t.string   "name",           null: false
    t.text     "text",           null: false
    t.integer  "menu_entity_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.index ["menu_entity_id"], name: "index_sub_pages_on_menu_entity_id", using: :btree
  end

  add_foreign_key "sub_pages", "menu_entities"
end
