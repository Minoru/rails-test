class CreateMenuEntities < ActiveRecord::Migration[5.0]
  def change
    create_table :menu_entities do |t|
      t.string :name, null: false
      t.string :link, index: true, unique: true, null: false
      t.text :text, null: false

      t.timestamps
    end
  end
end
