class CreateOrderServices < ActiveRecord::Migration[5.0]
  def change
    create_table :order_services do |t|
      t.string :full_name, null: false
      t.string :phone, null: false
      t.string :email, null: false
      t.text :comment

      t.timestamps
    end

    create_table :menu_entities_order_services, id: false do |t|
      t.belongs_to :menu_entity, index: true
      t.belongs_to :order_service, index: true
    end
  end
end
