class CreateSubPages < ActiveRecord::Migration[5.0]
  def change
    create_table :sub_pages do |t|
      t.string :name, null: false
      t.text :text, null: false
      t.references :menu_entity, foreign_key: true

      t.timestamps
    end
  end
end
